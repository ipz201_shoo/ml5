import numpy as np
import neurolab as nl
import numpy.random as rand
import pylab as pl

# Моделювання вхідних даних
skv = 0.05
centr = np.array([[0.2, 0.2], [0.4, 0.4], [0.7, 0.3], [0.2, 0.5]])
rand_norm = skv * rand.randn(100, 4, 2)
inp = np.array([centr + r for r in rand_norm])
inp.shape = (100 * 4, 2)
rand.shuffle(inp)

# Створення мережі Кохонена з 2 входами та 4 нейронами
net = nl.net.newc([[0.0, 1.0], [0.0, 1.0]], 4)

# Навчання мережі за правилом CWTA (200 ітерацій)
error = net.train(inp, epochs=200, show=20)

# Відображення графіків
pl.figure(1)
pl.title("Classification Problem")

# Графік зміни помилки
pl.plot(error)
pl.xlabel("Epoch number")
pl.ylabel("error (default MAE)")

# Графік вхідних даних та центрів кластерів
pl.figure(2)
pl.plot(inp[:, 0], inp[:, 1], ".", centr[:, 0], centr[:, 1], "yv", net.layers[0].np["w"][:, 0], net.layers[0].np["w"][:, 1], "p")
pl.legend(["train samples", "real centers", "train centers"])
pl.show()
