import numpy as np
import neurolab as nl
import pylab as pl

# Змінені вхідні дані згідно з варіантом
skv = 0.03
centr = np.array([[0.2, 0.2], [0.4, 0.4], [0.3, 0.3], [0.2, 0.6], [0.5, 0.7]])
rand_norm = skv * np.random.randn(100, 5, 2)
inp = np.array([centr + r for r in rand_norm])
inp.shape = (100 * 5, 2)
np.random.shuffle(inp)

# Створення нейронної мережі Кохонена з 2 входами і 5 нейронами
net = nl.net.newc([[0.0, 1.0], [0.0, 1.0]], 5)  # Змінено кількість нейронів на 5

# Тренування мережі за правилом CWTA на 200 ітерацій (епох) та виведення інформації
error = net.train(inp, epochs=200, show=20)

# Відображення графіків
pl.title('Classification Problem')
pl.subplot(211)
pl.plot(error)
pl.xlabel('Epoch number')
pl.ylabel('error (default MAE)')
w = net.layers[0].np['w']

pl.subplot(212)
pl.plot(inp[:, 0], inp[:, 1], '.', centr[:, 0], centr[:, 1], 'yv', w[:, 0], w[:, 1], 'p')
pl.legend(['train samples', 'real centers', 'train centers'])
pl.show()
