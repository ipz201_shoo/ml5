import numpy as np

def sigmoid(x):
    # Sigmoid activation function
    return 1 / (1 + np.exp(-x))

class Neuron:
    def __init__(self, weights, bias):
        self.weights = weights
        self.bias = bias

    def feedforward(self, inputs):
        total = np.dot(self.weights, inputs) + self.bias
        return sigmoid(total)

# Weights and biases
weights = np.array([0, 1])  # w1 = 0, w2 = 1
bias = 4  # b = 4

# Creating a neuron
n = Neuron(weights, bias)

# Input values
x = np.array([2, 3])  # x1 = 2, x2 = 3

# Calculate the output value
output = n.feedforward(x)

print(output)
